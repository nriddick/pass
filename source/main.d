/**	
	This work is free. You can redistribute it and/or modify it under the
	terms of the Do What The Fuck You Want To Public License, Version 2,
	as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

	Password generator written in D.

$pass N switches
default ASCII alphanumeric character domain
if no arguments passed, prints a 9 character password with at least one number,
	lowercase letter, and uppercase letter.
extend domain with -d[...] and -f[...] switches
aborts with exception if minimum number of characters exceeds N
	N              -- number of characters
	-help          -- display this message
	-d[characters] -- add characters to domain, may not be selected, no whitespace
	-f[characters] -- force at least one of given characters to appear, no whitespace
	-num           -- force 1+ number
	-up            -- force 1+ uppercase (ASCII)
	-low           -- force 1+ lowercase (ASCII)
	-tr            -- force 1+ top row (US) character
	-a or -all     -- force one of each above
ex: "pass 8 -f[-+=] -a" prints an 8-character string with at least one
number and top row char, upper/lower-case chars, and at least one char from "-+="
compatibility with non-ASCII sequences not guaranteed
*/
import std.random;
import std.stdio;
import std.string;
import std.exception;
import std.conv;
import std.regex;
import std.array;
import std.algorithm : sort;

string lower = "abcdefghijklmnopqrstuvwxyz";
string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
string number = "1234567890";
string tr = "!@#$%^&*()";

int main(string[] args) {
	args.popFront;
	if (args.length == 0) {
		return main(["sub","9","-num","-low","-up"]);
	}

	auto reg_help = regex(r"^(-h|-help)$");
	foreach (a; args) {
		auto m = match(a, reg_help);
		if (m) { help(); return 0; }
	}

	if (!isNumeric(args[0])) {
		writeln("ERR: no number passed");
		return 1;
	}
	
	int N = to!int(args[0]);
	if (N <= 0) {
		writeln("ERR: N must be greater than 0");
		return 1;
	}
	int minchars = 0;
	char[] pass; pass.length = N;
	args.popFront;
	
	char[] domain = to!(char[])(lower~upper~number);
	string[] add_force;
	foreach (a; args) parse(a, domain, add_force);
	minchars += add_force.length;
	if (minchars > N) {
		writeln("ERR: N must be >= the number of forced characters, "
			~minchars.to!string);
		return 1;
	}

	auto sortable = cast(uint[]) domain.to!dstring;
	sortable.sort();
	auto rectify = cast(dstring) sortable;
	rectify = rectify.squeeze;
	domain = cast(char[]) rectify.to!string; //domain all unique characters
	int[] forcepos;
	forcepos.length = add_force.length;
	if (add_force.length > 0) setForcePositions(forcepos, N);

	foreach (i, c; pass) pass[i] = domain[uniform(0,domain.length)];
	foreach (i, s; add_force)
		pass[forcepos[i]] = add_force[i][uniform(0,add_force[i].length)];
	write(pass);
	return 0;
}

private:

void help() {
write(helpstr);
}

void parse(string token, ref char[] domain, ref string[] add_force) {	
	auto reg_add = regex(r"^-[fd]\[\S+\]$");
	auto reg_flags = regex(r"^(-a|-all|-tr|-num|-up|-low)$");
	auto m1 = match(token,reg_add);
	if (m1) {
		auto cadd = m1.captures[0];
		auto chars = cadd[3..$-1];
		if (cadd[1] == 'f') {
			add_force ~= chars;
			domain ~= chars;
		}
		else domain ~= chars;
	}
	else {
		auto m2 = match(token,reg_flags);
		if (m2) {
			auto cflags = m2.captures[0];
			if (cflags[1] == 'a') {
				add_force ~= number;
				add_force ~= upper;
				add_force ~= lower;
				add_force ~= tr;
				domain ~= tr;
			}
			else if (cflags[1] == 'n') add_force ~= number;
			else if (cflags[1] == 'u') add_force ~= upper;
			else if (cflags[1] == 'l') add_force ~= lower;
			else {
				add_force ~= tr;
				domain ~= tr;
			}
		}
	}
}

void setForcePositions(ref int[] forcepos, int N) {
	bool[int] uniqs; //int keys and dummy values
	while (uniqs.length < forcepos.length) uniqs[uniform(0,N)] = true;
	forcepos = uniqs.keys;
}

immutable string helpstr = 
`$pass N switches
default ASCII alphanumeric character domain
if no arguments passed, prints a 9 character password with at least one number,
	lowercase letter, and uppercase letter.
extend domain with -d[...] and -f[...] switches
aborts with exception if minimum number of characters exceeds N
	N              -- number of characters
	-help          -- display this message
	-d[characters] -- add characters to domain, may not be selected, no whitespace
	-f[characters] -- force at least one of given characters to appear, no whitespace
	-num           -- force 1+ number
	-up            -- force 1+ uppercase (ASCII)
	-low           -- force 1+ lowercase (ASCII)
	-tr            -- force 1+ top row (US) character
	-a or -all     -- force one of each above
ex: "pass 8 -f[-+=] -a" prints an 8-character string with at least one
number and top row char, upper/lower-case chars, and at least one char from "-+="
compatibility with non-ASCII sequences not guaranteed
`;

unittest {
	assert(main(["dummy","8","-a"]) == 0);
	assert(main(["dummy","2","-a"]) == 1);
	writeln(main(["dummy","8","-a"]));
	writeln(main(["dummy","8","-a"]));
	writeln(main(["dummy","8","-a"]));
	writeln(main(["dummy","8","-up"]));
	writeln(main(["dummy","8","-up"]));
	writeln(main(["dummy","8","-up"]));
	writeln(main(["dummy","8","-low"]));
	writeln(main(["dummy","8","-low"]));
	writeln(main(["dummy","8","-low"]));
	writeln(main(["dummy","8","-num"]));
	writeln(main(["dummy","8","-num"]));
	writeln(main(["dummy","8","-num"]));
	writeln(main(["dummy","8","-tr"]));
	writeln(main(["dummy","8","-tr"]));
	writeln(main(["dummy","8","-tr"]));
}